const express = require('express');
const router = express.Router();
const { flashcards } = require('../data/flashcardData.json');

const getFlashcardThemes = () => {
    const themesArray = [];
    for (let flashcard of flashcards) {
        themesArray.push(flashcard.theme);
    }
    return themesArray;
};

const getFlashcards = (theme) => {
    const cardsArray = [];
    for (let flashcard of flashcards) {
        if (flashcard.theme === theme) {
            cardsArray.push(flashcard.cards);
        }
    }
    return cardsArray;
};

router.get('/', (req, res) => {
    if (!req.cookies.theme) {
        res.redirect('/');
    } else {
        const username = req.cookies.username;
        const chosenTheme = req.cookies.theme;
        const allThemes = getFlashcardThemes();
        const matchedTheme = allThemes.filter((theme) => {
            return theme === chosenTheme;
        });
        const userTheme = matchedTheme[0];
        const cards = getFlashcards(userTheme)[0];
        const templateData = { matchedTheme, username, cards };
        
        res.render('review', templateData);
    }
    // const newCardId = getRandomCardId(cards.length);
    // res.redirect(`/cards/${newCardId}/?side=question`);
});

// router.get('/:id', (req, res) => {
//     const { side = 'question' } = req.query;
//     const { id } = req.params;
//     const text = cards[id][side];
//     const { hint } = cards[id];
//     const { username, theme } = req.cookies;
    
//     const templateData = { id, theme, text, username };

//     if ( side === 'question') { 
//         templateData.hint = hint;
//         templateData.sideToShow = 'answer';
//         templateData.sideToShowDisplay = 'Answer';
//         res.render('card', templateData);
//     } else if ( side === 'answer' ) {
//         templateData.sideToShow = 'question';
//         templateData.sideToShowDisplay = 'Question';
//         res.render('card', templateData);
//     } else {
//         res.redirect('/');
//     }
// });

module.exports = router;