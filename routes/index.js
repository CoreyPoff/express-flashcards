const express = require('express');
const router = express.Router();
const browser = require('browser-detect');
const { flashcards } = require('../data/flashcardData.json');

const getFlashcardThemes = () => {
    const themesArray = [];
    for (let flashcard of flashcards) {
        themesArray.push(flashcard.theme);
    }
    return themesArray;
};

// If username exists, render the home page (pass username and themes with it)
// Else send user to hello page
router.get('/', (req, res) => {
    const username = req.cookies.username;
    const themes = getFlashcardThemes();
    if (username) {
        res.render('home', { username, themes });
    } else {
        res.redirect('/hello');
    }
});

// If username already exists, redirect to home page
// Else if current browser is IE, render hello page... sort of
// Else render hello page
router.get('/hello', (req, res) => {
    const currBrowser = browser(req.headers['user-agent']);
    const username = req.cookies.username;

    if (username) {
        res.redirect('/');
    } else if (currBrowser.name === 'ie') {
        console.log('you are using IE');
        res.render('hello', { browserIsIE: true });
    } else {
        res.render('hello', { loginView: true });
    }
});

// On form submit, set username cookie,
// then redirect to home page
router.post('/hello', (req, res) => {
    res.cookie('username', req.body.username);
    res.redirect('/');
});

// On form submit, set theme cookie,
// then redirect to cards page
router.post('/', (req, res) => {
    res.cookie('theme', req.body.theme);
    res.redirect('/review');;
});

// On form submit, clear username cookie
// then redirect to hello page
router.post('/goodbye', (req, res) => {
    res.clearCookie('username');
    res.clearCookie('theme');
    res.redirect('/hello');
});

module.exports = router;