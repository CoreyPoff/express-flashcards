const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const port = process.env.PORT || 3000;

const app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/static', express.static('public'));

app.set('view engine', 'handlebars');

app.use(express.static(path.join(__dirname, '/public')));

const mainRoutes = require('./routes');
const reviewRoutes = require('./routes/review');

app.use(mainRoutes);
app.use('/review', reviewRoutes);

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    res.locals.error = err;
    res.status(err.status);
    res.render('error');
});

app.listen(port, () => {
    console.log("Listening on port " + port);
});