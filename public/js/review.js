const selectors = {
    flashcard: '.flashcard',
    nextBtn: '.js-next-btn',
    cardCount: '.js-card-count'
};

const firstFlashcard = document.querySelector(selectors.flashcard);
const allFlashcards = document.querySelectorAll(selectors.flashcard);
const allNextBtns = document.querySelectorAll(selectors.nextBtn);
const cardCount = document.querySelector(selectors.cardCount);

const getSiblingsOf = (el, filter) => {
    var sibs = [];
    while (el = el.nextElementSibling) {
        if (el.nodeType === 3) continue; // text node
        if (!filter || filter(el)) sibs.push(el);
    }
    return sibs;
};

const updateCardCount = (currCard, numToAdd) => {
    const numSibs = getSiblingsOf(currCard);
    const totalCards = numSibs.length + numToAdd;
    cardCount.innerHTML = totalCards;
};

const hideCurrentCard = (event) => {
    event.stopPropagation();
    const currentFlashcard = event.target.closest('.flashcard');
    currentFlashcard.classList.add('flashcard--hidden');
    updateCardCount(currentFlashcard, 0);
}; 

const getNextCard = (event) => {
    const currentFlashcard = event.target.closest('.flashcard');
    const allSibs = getSiblingsOf(currentFlashcard);
    const numToSubtract = 39;
    
    allSibs.forEach((sib) => {
        // Get sibling's current left value
        const currLeft = parseInt(window.getComputedStyle(sib).getPropertyValue('left'));
        // Do some subtraction
        const newLeft = currLeft - numToSubtract;

        // Set sibling's new left value, so it "scoots" over 
        Object.assign(sib.style, {
            left: `${newLeft}px`,
            transition: '0.3s',
            transitionDelay: '0s'
        });
    });

    currentFlashcard.nextElementSibling.classList.add('flashcard--active');
    currentFlashcard.classList.remove('flashcard--active');
};

updateCardCount(firstFlashcard, 1);

// Slide cards in
allFlashcards.forEach((flashcard) => {
    setTimeout(() => {
        flashcard.classList.add('flashcard--spread');
    }, 25);
});

// Flip cards
allFlashcards.forEach((flashcard) => {
    flashcard.addEventListener('click', (event) => {
        if (flashcard.classList.contains('flashcard--flipped')) {
            flashcard.classList.remove('flashcard--flipped');
        } else {
            flashcard.classList.add('flashcard--flipped');
        }
    });
});

// Dismiss cards
allNextBtns.forEach((nextBtn) => {
    nextBtn.addEventListener('click', (event) => {
        const currentFlashcard = event.target.closest('.flashcard');
        hideCurrentCard(event);
        if (currentFlashcard.nextElementSibling) {
            getNextCard(event);
        } else {
            document.querySelector('.review__header').classList.add('h-fade-out');
            document.querySelector('.try-again').classList.add('try-again--active');
        }
    });
});