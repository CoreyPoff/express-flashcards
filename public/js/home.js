const selectors = {
    themeForm: '.theme-selection__form',
    themeBtn: '.js-theme-btn',
    countdown: '.countdown'
};

const themeForm = document.querySelector(selectors.themeForm);
const themeBtn = document.querySelector(selectors.themeBtn);
const countdown = document.querySelector(selectors.countdown);

themeForm.addEventListener('change', () => {
    themeBtn.disabled = false;
});

// Delay form submission until countdown animation is complete
themeForm.addEventListener('submit', (event) => {
    event.preventDefault();
    countdown.classList.add('countdown--start');
    setTimeout(() => {
        themeForm.submit();
    }, 3000);
});