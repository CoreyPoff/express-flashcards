# express-flashcards

A flashcard game built with Express and vanilla JavaScript. If you want to give it a whirl, just clone the repo and follow these instructions:

```
cd express-flashcards
npm install
npm run start
```

Go to `localhost:3000` in your browser, and _voila!_ you're ready to play.

P.S. This game does not work in IE. If you try to open it in IE, Captain Picard will - I repeat, _will_ - swear at you.
